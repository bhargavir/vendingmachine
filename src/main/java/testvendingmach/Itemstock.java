package testvendingmach;

public class Itemstock {
	Items item;
	int qtyavailable;

	public Itemstock(Items item, int qty){
		this.item = item;
		qtyavailable = qty;
	}
	
	public Items getItem(){
		return item;
	}
	
	public int getQtyavailable(){
		return qtyavailable;
	}
	
	public void updateQtydown(){
		qtyavailable--;
	}
	
}
