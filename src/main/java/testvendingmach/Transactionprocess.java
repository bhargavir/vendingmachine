package testvendingmach;

import java.util.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class Transactionprocess {

	Exchangeinventory cashinv;
	Vendorinventory iteminv;
	Scanner scanner;
	
	@Autowired
	public Transactionprocess(@Qualifier("itemsinventry")Iteminventory iteminv, @Qualifier("cashinventry")Cashinventory cashinv){
		this.cashinv = cashinv;
		this.iteminv = iteminv;
		scanner = new Scanner(System.in);
	}
	
	//public Transactionprocess(){
	//	cashinv = new Cashinventory();
	//	iteminv = new Iteminventory();
	//	scanner = new Scanner(System.in);
	//}
	
	public String displayOption(){
		
		String item;
		System.out.println("Enter the item you want from the list ");
		int i = 0;
		String opt = iteminv.showList(i);
		while (!opt.equals("X"))
		{	
			System.out.println(opt);
			i++;
			opt = iteminv.showList(i);
		 } 
		
		System.out.println("Enter 'X' to Exit ");
		item = scanner.nextLine();
		return item;
	}
	
	public void processTransaction(String itemname){
		
		List<Cashcount> tmpcashinv = new ArrayList<Cashcount>();
		int count = 0;
		int cash = 0;
		
		System.out.println("Enter the denomination ");
		int denomination = scanner.nextInt();
		scanner.nextLine();
		while (denomination !=0){
			System.out.println("Enter the Count for " + denomination);
			count = scanner.nextInt();
			scanner.nextLine();
			cash = cash + (denomination * count);
			Cashcount tmpcashcount = new Cashcount(denomination,count);
			tmpcashinv.add(tmpcashcount);
		}
		
		int cost = iteminv.chkItemavailable(itemname);
		if (cost > 0){
			boolean cashavbl = cashinv.chkBalAvailable(cash,cost, tmpcashinv);
			if (cashavbl){
				iteminv.updateItemquantity(itemname);
			}
			else 
			{
				System.out.println("No change available ");
			}
		}
	}
	
	public void exitprocess(){
		 scanner.close();
	   }
}

