package testvendingmach;

public class Items {
     String itemid;
     String itemname;
     int cost;

     public Items(String itemid, String itemname, int cost){
    	 this.itemid = itemid;
    	 this.itemname = itemname;
    	 this.cost = cost;
    	 
     }

     public String getItemid(){
    	 return itemid;
     }
     
     public String getItemname(){
    	 return itemname;
     }
     
     public int getCost(){
    	 return cost;
     }
     
     public void setItemid(String itemid){
    	 this.itemid = itemid;
     }
     
     public void setItemname(String itemname){
    	 this.itemname = itemname;
     }
     
     public void setCost(int cost){
    	 this.cost = cost;
     }
}
