package testvendingmach;
import java.util.*;
import org.springframework.stereotype.Component;

@Component("cashinventry")
public class Cashinventory implements Exchangeinventory{
	List<Cashcount> cashavailable;
	
	public Cashinventory(){
		//Comparator<Cashcount> cashcompare = Comparator.comparing(Cashcount::getDenomination);
		//SortedSet<Cashcount> cashavailable = new TreeSet<>();
		SortedSet<Cashcount> cashavailable = new TreeSet<>(Comparator.comparing(Cashcount::getDenomination));
		//cashavailable = new ArrayList<Cashcount>();
		Cashcount tmpcash = new Cashcount(100,5);
		cashavailable.add(tmpcash);
		tmpcash = new Cashcount(20,5);
		cashavailable.add(tmpcash);
		tmpcash = new Cashcount(10,5);
		cashavailable.add(tmpcash);
		tmpcash = new Cashcount(50,5);
		cashavailable.add(tmpcash);
		tmpcash = new Cashcount(5,5);
		cashavailable.add(tmpcash);
		tmpcash = new Cashcount(1,5);
		cashavailable.add(tmpcash);
	}
	
	public boolean chkBalAvailable(int cash, int cost, List<Cashcount> cashinv){
		//System.out.println("H1");
		boolean avbl = false;
		int balance = cash-cost;
		for (Cashcount tmpcash : cashavailable){
			//System.out.println("H2"); 
			int rem = balance / tmpcash.getDenomination();
			 balance = balance % tmpcash.getDenomination();
			 if (rem > tmpcash.getCount()){
				 int diff = rem - tmpcash.getCount();
				 balance = balance + (diff * tmpcash.getDenomination()); 
			 }
		}
		if (balance == 0){
			avbl = true;
			updateCashDetails(cash, cost, cashinv);
		}
		
		return avbl;
	}

	private void updateCashDetails(int cash, int cost, List<Cashcount> cashinv){
		
		int balance = cash-cost;
		int qty = 0;
		if (balance > 0){
			for (Cashcount tmpcash : cashavailable){
			System.out.println("Denomination " + tmpcash.getDenomination());
			 if (balance > tmpcash.getDenomination()){
				 int rem = balance / tmpcash.getDenomination();
				 balance = balance % tmpcash.getDenomination();
				 if (rem > tmpcash.getCount()){
					 int diff = rem - tmpcash.getCount();
					 balance = balance + (diff * tmpcash.getDenomination());
					 qty = tmpcash.getCount();
				 }
				 else {
					   qty = rem;
				 }
				 System.out.println("Denomination " + tmpcash.getDenomination() + "is given" + qty); 
				 tmpcash.updateCountDown(qty);
				 updateincomingcashininv(cashinv);
				 
			 }
			 
		}
		}
	}
	private void updateincomingcashininv(List<Cashcount> cashinv){
		for (Cashcount tmpcash : cashavailable){
			for (Cashcount tmpcashinv : cashinv){
				if (tmpcash.getDenomination() == tmpcashinv.getDenomination()){
					tmpcash.updateCountUp(tmpcashinv.getCount());
				}
			}
		}
	}
}
