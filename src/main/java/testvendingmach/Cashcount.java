package testvendingmach;

public class Cashcount {
    int denomination;
    int count;
   
    public Cashcount(int denomination, int count){
    	this.denomination = denomination;
    	this.count = count;
    }
    
    public int getDenomination(){
    	return denomination;
    }
    
    public int getCount(){
    	return count;
    }
    
    public void setDenomination(int denomination){
    	this.denomination = denomination;
    }
    
    public void updateCountUp(int qty){
       	 count = count + qty;
    }
    
    public void updateCountDown(int qty){
      	 count = count - qty;
   }
}

