package testvendingmach;
import java.util.*;
import org.springframework.stereotype.Component;
@Component("itemsinventry")
public class Iteminventory implements Vendorinventory{
	
	List<Itemstock> itemsinv;
	
	public Iteminventory(){
		
		itemsinv = new ArrayList<Itemstock>();
		
		Items tmpitem = new Items("Choco" , "Choco", 5);
		Itemstock tmpitemstock = new Itemstock(tmpitem,5);
		itemsinv.add(tmpitemstock);
		
		tmpitem = new Items("Chips" , "Chips", 10);
		tmpitemstock = new Itemstock(tmpitem,5);
		itemsinv.add(tmpitemstock);
		
	}
	
	public String showList(int itemind){
		String itemname = "X";
		if (itemind < itemsinv.size()){
			itemname = itemsinv.get(itemind).getItem().getItemname();
		}
		 return itemname;
	}
	
	public int chkItemavailable(String itemid){
		int cost = 0;
		for (Itemstock istock : itemsinv){
			//System.out.println("H4");
			if (istock.getItem().getItemid().equals(itemid)){
				if (istock.getQtyavailable() > 0){
					cost = istock.getItem().getCost();
					break;
				}
			}
		}
		
		return cost;
	}
	
	public void updateItemquantity(String itemid){
		
		for (Itemstock istock : itemsinv){
			//System.out.println("H5");
			if (istock.getItem().getItemid().equals(itemid)){
				istock.updateQtydown();
		
			}
		}		
	}
	
	
}
