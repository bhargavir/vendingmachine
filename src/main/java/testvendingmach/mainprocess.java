package testvendingmach;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class mainprocess {

	public static void main(String[] args) {
		
		ApplicationContext ctx = new ClassPathXmlApplicationContext("classpath*:spring-context.xml");
		Transactionprocess process = ctx.getBean(Transactionprocess.class);
		
		//Transactionprocess process = new Transactionprocess();
		
		String item = "";
		item = process.displayOption();
		while (!item.equals("X")){
			process.processTransaction(item);
			item = process.displayOption();
		}
        process.exitprocess();
        ((ClassPathXmlApplicationContext)ctx).close();
	}

}
