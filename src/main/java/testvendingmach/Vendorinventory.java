package testvendingmach;

public interface Vendorinventory {
	
	public String showList(int itemind);
	public int chkItemavailable(String itemid);
	public void updateItemquantity(String itemid);
	
}
